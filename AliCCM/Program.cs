using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.SS.Util;
using System.IO;
using System.Diagnostics;

namespace AliCCM
{
    class Program
    {
        static void Main(string[] args)
        {
            //There will be a file name AD_group.txt in the OKresults folder.
            //Name the file using this.
            

                // The code provided will print ‘Hello World’ to the console.
                // Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.
            Console.WriteLine("Checkout Ali's CCM");
            Console.WriteLine("Beginning Sheet Creation");
            string directoryPath = Directory.GetParent(Directory.GetCurrentDirectory()).ToString();
            string appListPath = directoryPath + "\\App_list.csv";
            string tempAppListPath = Directory.GetCurrentDirectory() + "\\Applist.csv";
            string computerListPath = directoryPath + "\\List.csv";
            string tempComputerListPath = Directory.GetCurrentDirectory() + "\\List.csv";
            
            DirectoryInfo uncontactableDirectory = null;
            if (Directory.Exists(directoryPath + "\\NotReachable_Results"))
                uncontactableDirectory = new DirectoryInfo(directoryPath + "\\NotReachable_Results");
            DirectoryInfo failureDirectory = null;
            if (Directory.Exists(directoryPath + "\\CheckResults"))
                failureDirectory = new DirectoryInfo(directoryPath + "\\CheckResults");
            DirectoryInfo oKDirectory = null;
            if (Directory.Exists(directoryPath + "\\OKResults"))
                oKDirectory = new DirectoryInfo(directoryPath + "\\OKResults");
            int colTotal = 0;
            int colComputers = 1;
            int colWinVer = 2;
            int colFails = 3;
            int colFailList = 4;
            int colSoftwareStart = 5;

            List<Software> softwareList = new List<Software>();
            List<String> uncontactableList = new List<String>();
            List<Computer> computers = new List<Computer>();
            string ouName = "Anonymous"; 
            //GRABBING THE NAME HERE
            using (StreamReader streamer = new StreamReader(directoryPath + "\\OKResults\\AD_group.txt"))
            {
                ouName = streamer.ReadLine();
            }
            string filePath = directoryPath + "\\"+ouName+" Summary.xlsx";

            
            //Console.ReadKey();
            //Prepare Skeleton of Form
            //IE Top Excel Sheet
            //Delet Previous one
            UsabilityCheck();
            
            using (FileStream stream = new FileStream(filePath, FileMode.Create, FileAccess.ReadWrite))
            {

                Console.WriteLine("Reading in Program Names");
                IWorkbook workBook = new XSSFWorkbook();
                ISheet sheet = workBook.CreateSheet("Summary");

                IFont font = workBook.CreateFont();
                font.FontHeightInPoints = 11;
                font.FontName = "Arial";
                font.Boldweight = (short)FontBoldWeight.Bold;
                var style = workBook.CreateCellStyle();
                style.SetFont(font);
                IRow row = sheet.CreateRow(0);

                ICell cell = row.CreateCell(colComputers);
                cell.SetCellValue("Computers");
                cell.CellStyle = style;
                row.RowStyle = style;

                int column = row.LastCellNum;
                sheet.AutoSizeColumn(column);

                #region Cell Colour Styles
                var greenColourCell = workBook.CreateCellStyle();
                greenColourCell.FillForegroundColor = IndexedColors.Green.Index;
                greenColourCell.FillPattern = FillPattern.SolidForeground;

                var redColourCell = workBook.CreateCellStyle();
                redColourCell.FillForegroundColor = IndexedColors.Red.Index;
                redColourCell.FillPattern = FillPattern.SolidForeground;

                var yellowColourCell = workBook.CreateCellStyle();
                yellowColourCell.FillForegroundColor = IndexedColors.Yellow.Index;
                yellowColourCell.FillPattern = FillPattern.SolidForeground;
                #endregion


                int softwareCounter = 0;
                using (var inputFile = new FileStream(appListPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    using (var outputFile = new FileStream(tempAppListPath, FileMode.Create))
                    {
                        var buffer = new byte[0x10000];
                        int bytes;

                        while ((bytes = inputFile.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            outputFile.Write(buffer, 0, bytes);
                        }
                    }
                }
                using (StreamReader streamer = new StreamReader(tempAppListPath))
                {
                    string line;
                    string softName;
                    while ((line = streamer.ReadLine()) != null)
                    {
                        //Skips the empty lines
                        
                        softName = line.Split(new Char[] { '\t', ',' })[0];

                        if (string.IsNullOrWhiteSpace(softName) || string.IsNullOrEmpty(softName))
                        {
                            Console.WriteLine("We have a blank");
                            continue;
                        }
                        cell = row.CreateCell(softwareCounter + colSoftwareStart);
                        cell.SetCellValue(softName);
                        cell.CellStyle = style;
                        column = row.LastCellNum;
                        sheet.AutoSizeColumn(column);

                        softwareList.Add(new Software(softName));
                        softwareCounter++;
                    }

                    cell = row.CreateCell(colFails);
                    cell.SetCellValue("Fails");
                    cell.CellStyle = style;
                    column = row.LastCellNum;
                    sheet.AutoSizeColumn(column);

                    cell = row.CreateCell(colFailList);
                    cell.SetCellValue("Fail List");
                    cell.CellStyle = style;
                    column = row.LastCellNum;
                    sheet.AutoSizeColumn(column);

                    streamer.Close();
                    streamer.Dispose();
                }
                //NEED FIXING
                File.Delete(tempAppListPath);
                Console.WriteLine("Compiling Contactables");
                //Make Uncontactable List
                if (uncontactableDirectory != null)
                {
                    if (uncontactableDirectory.EnumerateFiles().Count() > 0)
                    {
                        string uncontactableListPath = uncontactableDirectory.EnumerateFiles().ToList()[0].ToString();
                        //Console.WriteLine(uncontactableListPath.ToString());
                        using (StreamReader streamer = new StreamReader(uncontactableDirectory.ToString() + "\\" + uncontactableListPath))
                        {
                            string line;
                            while ((line = streamer.ReadLine()) != null)
                            {
                                string compName = line.Split(new Char[] { '\t', ',' })[0];
                                uncontactableList.Add(compName);
                                //Console.WriteLine("Confirmed Uncontactable Machine " + compName);
                            }
                            streamer.Close();
                            streamer.Dispose();
                        }
                    }
                }
                using (var inputFile = new FileStream(computerListPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    using (var outputFile = new FileStream(tempComputerListPath, FileMode.Create))
                    {
                        var buffer = new byte[0x10000];
                        int bytes;

                        while ((bytes = inputFile.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            outputFile.Write(buffer, 0, bytes);
                        }
                    }
                }
                using (StreamReader streamer = new StreamReader(tempComputerListPath))
                {
                    string line;
                    while ((line = streamer.ReadLine()) != null)
                    {
                        string compName = line.Split(new Char[] { '\t', ',' })[0];
                        if (!(uncontactableList.Contains(compName)))
                        {
                            Computer tempComputer = new Computer(compName, softwareList);
                            computers.Add(tempComputer);
                        }/*
                        else
                        {
                            //Console.WriteLine("Confirmed Uncontactable in list "+compName);
                        }*/
                    }
                    streamer.Close();
                    streamer.Dispose();
                }
                
                File.Delete(tempComputerListPath);
                Console.WriteLine("Celebrating Successes");
                //Goes through Failure Lists and removes the failures
                if (failureDirectory != null)
                {
                    if (failureDirectory.EnumerateFiles().Count() > 0)
                    {
                        List<FileInfo> failureFiles = failureDirectory.EnumerateFiles().ToList();
                        foreach (FileInfo failureFile in failureFiles)
                        {
                            string failure = failureFile.ToString().Replace("Check", "");
                            failure = failure.Replace(".csv", "");
                            //Console.WriteLine(failureFile.ToString());
                            using (StreamReader streamer = new StreamReader(failureDirectory.ToString() + "\\" + failureFile.ToString()))
                            {
                                string line;
                                while ((line = streamer.ReadLine()) != null)
                                {
                                    string compName = line.Split(new Char[] { '\t', ',' })[0];
                                    //Console.WriteLine("Checking on " + compName);
                                    //Computer computer = computers.Where(comp => comp.name.Contains(compName)).FirstOrDefault();
                                    foreach (Computer computer in computers)
                                    {
                                        if (string.Equals(computer.name, compName))
                                        {
                                            Software failedSoftware = softwareList.Find(x => x.name.Equals(failure));
                                            computer.software[failedSoftware] = false;
                                            computer.iterateFails();
                                            failedSoftware.iterateFails();
                                            /*if (computer.software[failure])
                                                Console.WriteLine("Don't trust me. I didn't write.");*/
                                            //Console.WriteLine("Match Found for " + failure +" on PC "+compName+". Now has " +computer.fails+" fails");
                                            break;
                                        }
                                    }
                                }
                                streamer.Close();
                                streamer.Dispose();
                            }
                        }
                    }
                }
                Console.WriteLine("Failed Software Accounted Successfully. Now Creating Spreadsheet");


                using (StreamReader streamer = new StreamReader(oKDirectory + "\\Winver.csv"))
                {
                    string line;
                    while ((line = streamer.ReadLine()) != null)
                    {
                        string compName = line.Split(new Char[] { '\t', ',' })[0];
                        string winVer = line.Split(new Char[] { '\t', ',' })[1];
                        if (!(winVer.Equals("Build 17763")))
                        {
                            Console.WriteLine(compName+" has an incorrect Windows Version!");
                            computers.Find(x =>  x.name.Equals(compName)).winver=false;
                                //winver = false;
                        }
                    }
                }
                softwareList = softwareList.OrderByDescending(o => o.fails).ToList();

                for (int x = 0; x<softwareCounter; x++)
                { 
                    cell = row.GetCell(x + colSoftwareStart);
                    cell.SetCellValue(softwareList[x].name);
                    cell.CellStyle = style;
                    column = x+colSoftwareStart;
                    sheet.AutoSizeColumn(column);
                }

                cell = row.CreateCell(colWinVer);
                cell.SetCellValue("WinVer");
                cell.CellStyle = style;
                //Moving On
                row = sheet.CreateRow(1);
                cell = row.CreateCell(colTotal);
                cell.SetCellValue("Total");
                column = 0;
                sheet.AutoSizeColumn(column);
                cell.CellStyle = style;

                cell = row.CreateCell(colComputers);
                cell.SetCellType(CellType.Numeric);
                int totalComputers = uncontactableList.Count() + computers.Count();
                Console.WriteLine("There are " + uncontactableList.Count() + " Uncontactable Computers");
                Console.WriteLine("There are " + computers.Count() + " Contactable Computers");
                cell.SetCellValue(totalComputers);

                cell = row.CreateCell(colWinVer);
                cell.SetCellType(CellType.Formula);
                string k = CellReference.ConvertNumToColString(cell.ColumnIndex);
                string tempoooo = "COUNTIF(" + k + 3 + ":" + k + (3 + totalComputers) + ",\"Fail\")";
                cell.SetCellFormula(tempoooo);

                for (int x = 0; x < softwareCounter; x++)
                {
                    cell = row.CreateCell(x + colSoftwareStart);
                    cell.SetCellType(CellType.Formula);
                    string i = CellReference.ConvertNumToColString(cell.ColumnIndex);
                    string tempe = "COUNTIF(" + i + 3 + ":" + i + (3 + totalComputers) + ",\"Fail\")";
                    cell.SetCellFormula(tempe);
                    //Console.WriteLine(tempe);
                }

                cell = row.CreateCell(colFails);
                cell.SetCellType(CellType.Formula);
                string j = CellReference.ConvertNumToColString(cell.ColumnIndex);
                string tempo = "SUM(" + j + 3 + ":" + j + (3 + totalComputers) + ")";
                cell.SetCellFormula(tempo);


                int countdown = 2;
                //Final list of Computers.
                //Sorting so most fails on top.
                computers = computers.OrderByDescending(o => o.fails).ToList();
                //computers.Sort((x, y) => x.fails.CompareTo(y.fails));
                foreach (Computer computer in computers)
                {
                    row = sheet.CreateRow(countdown);
                    cell = row.CreateCell(colComputers);
                    cell.SetCellValue(computer.name);
                    string failList = "";
                    int x = 0;
                    foreach (Software software in softwareList)
                    {
                        cell = row.CreateCell(x+colSoftwareStart);
                        x += 1;
                        if (computer.software[software])
                        {
                            cell.SetCellValue("Success");
                            cell.CellStyle = greenColourCell;
                        }
                        else
                        {
                            cell.SetCellValue("Fail");
                            cell.CellStyle = redColourCell;
                            failList += software.name + " ";
                        }
                    }
                    cell = row.CreateCell(colFails);
                    cell.SetCellType(CellType.Formula);

                    j = CellReference.ConvertNumToColString(softwareCounter +colSoftwareStart);
                    tempo = "COUNTIF(E" + (countdown + 1) + ":" + j + (countdown + 1) + ",\"Fail\")";
                    cell.SetCellFormula(tempo);

                    x += 1;
                    cell = row.CreateCell(colFailList);
                    cell.SetCellValue(failList);

                    cell = row.CreateCell(colWinVer);
                    if (computer.winver)
                    {
                        cell.SetCellValue("Success");
                        cell.CellStyle = greenColourCell;
                    }
                    else
                    {
                        cell.SetCellValue("Fail");
                        cell.CellStyle = redColourCell;
                    }
                    countdown += 1;
                    
                }

                foreach (String computer in uncontactableList)
                {
                    row = sheet.CreateRow(countdown);
                    cell = row.CreateCell(1);
                    cell.SetCellValue(computer);

                    int x = 2;
                    foreach (Software software in softwareList)
                    {
                        cell = row.CreateCell(x);
                        x += 1;
                        cell.SetCellValue("N/A");
                        cell.CellStyle = yellowColourCell;
                    }
                    cell = row.CreateCell(x);
                    x += 1;
                    cell.SetCellValue("N/A");
                    cell.CellStyle = yellowColourCell;
                    countdown += 1;
                }

                for (int x = 0; x < softwareList.Count() + colSoftwareStart; x++)
                {
                    sheet.AutoSizeColumn(x);
                }

                row = sheet.GetRow(0);
                cell = row.CreateCell(colTotal);
                cell.CellStyle = yellowColourCell;
                cell.SetCellValue(uncontactableList.Count());

                sheet.CreateFreezePane(0, 2, 0, 2);

                //sheet.CreateFreezePane(softwareCounter + 2, 0);

                workBook.Write(stream);
                stream.Close();
                stream.Dispose();
                //Setting Column Autosize

            }
            System.Diagnostics.Process.Start(filePath);
            //Console.ReadKey();
            //This is to make sure no one is usuing the file
            void UsabilityCheck()
            {
                bool deletable = false;
                if (File.Exists(filePath))
                {
                    try
                    {
                        using (Stream stream = new FileStream(filePath, FileMode.Open))
                        {
                            Console.WriteLine(filePath + " already exists and is being deleted to make way for new version");
                            deletable = true;
                        }
                    }
                    catch
                    {
                        Console.WriteLine("!!!!!!!!!!!!WARNING!!!!!!!!! ");
                        Console.WriteLine(filePath + " already exists but is in use. Please close this document and press enter");
                        Console.ReadKey();
                        UsabilityCheck();
                        //check here why it failed and ask user to retry if the file is in use.
                    }
                    if (deletable)
                        File.Delete(filePath);
                }
                return;
            }
        }

        

        public class Computer
        {
            public string name;
            public Dictionary<Software, bool> software;
            public int fails;
            public bool winver;

            public Computer(string name, List<Software> softwareNames)
            {
                fails = 0;
                this.name = name;
                software = new Dictionary<Software, bool>();
                foreach (Software softName in softwareNames)
                {
                    software.Add(softName, true);
                }
                winver = true;
            }
            public void iterateFails()
            {
                fails += 1;
            }
        }

        public class Software
        {
            public string name;
            public int fails;
            public Software(string name)
            {
                this.name = name;
                fails = 0;
            }
            public void iterateFails()
            {
                fails += 1;
            }
        }
    }
}
